<?php

/**
 * @file
 * Enables the TinyMCE editor to work on pages that use AHAH.
 */
 
/**
 * Implementation of hook_elements().
 */
function tinymce_ahah_elements() {
  $aType = array();
  if (user_access('access tinymce')) {
    $aType['textarea'] = array('#process' => array('tinymce_ahah_process_textarea'));
  }
  return $aType;
}

/**
 * Add tinymce editor settings to Drupal.settings for each textarea where
 * tinymce editing is enabled.
 *
 * This is intended to work regardless of whether this function is run 
 * before or after tinymce_process_textarea().
 */
function tinymce_ahah_process_textarea($aElement) {
  global $user;
  static $sProfileName;
  static $aSettingsAdded = array();
  static $bJsAdded;
  
  // Get the profile appropriate for the user. If there isn't one, or if we're
  // not on a page that the profile is setup to use tinymce on, return.
  if (!$sProfileName) {
    $sProfileName = db_result(db_query('SELECT s.name FROM {tinymce_settings} s INNER JOIN {tinymce_role} r ON r.name = s.name WHERE r.rid IN (%s)', implode(',', array_keys($user->roles))));
    if (!$sProfileName) {
      return $aElement;
    }
  }
  $oProfile = tinymce_profile_load($sProfileName);
  if (!_tinymce_page_match($oProfile)) {
    return $aElement;
  }
  
  // Get the tinymce settings appropriate for this element. If there aren't any,
  // it means we're not using tinymce for this element, so return. We determine
  // the element name the same way tinymce.module does, even though it seems
  // less than ideal.
  $sElementId = $aElement['#id'];
  $sElementName = substr($sElementId, strpos($sElementId, '-') + 1);
  $aInit = tinymce_config($oProfile);
  $aInit['elements'] = $sElementId;
  $aInit = (array) theme('tinymce_theme', $aInit, $sElementName, $aInit['theme'], $bIsRunning);
	if (count($aInit) < 1) {
    return $aElement;
  }
  
  // Load our javascript file
  if (!$bJsAdded) {
    drupal_add_js(drupal_get_path('module', 'tinymce_ahah') . '/tinymce_ahah.js');
    $bJsAdded = TRUE;
  }
  
  // Add the settings to Drupal.settings, in the format that tinyMCE.init() will
  // want them. Need to only do this once per element id, because of the way the 
  // settings are merged at the end of the request.
  if (!$aSettingsAdded[$sElementId]) {
    $aSettings = array();
    foreach ($aInit as $k => $v) {
      if (is_array($v)) {
        $v = implode(',', $v);
      }
      if (strtolower($v) === 'true') {
        $v = TRUE;
      }
      if (strtolower($v) === 'false') {
        $v = FALSE;
      }
      $aSettings[$k] = $v;
    }
    drupal_add_js(array('tinymceAhah' => array($sElementId => $aSettings)), 'setting');
    $aSettingsAdded[$sElementId] = TRUE;
  }

  return $aElement;
}
