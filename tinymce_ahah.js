/**
 * Behavior for setting up tinymce editors and triggering their save
 * function from AJAX form submission.
 */
Drupal.behaviors.tinymceAhah = function(context) {
  // Ensure tiny_mce.js has been able to construct its basic API objects.
  // Otherwise, no point in continuing.
  if (!window.tinyMCE || !window.tinymce || !window.tinymce.dom || !window.tinymce.dom.Event) {
    return;
  }
  
  // Tiny_mce.js may need to load additional scripts (themes, plugins, etc.), and to
  // do so properly, it needs to recognize the page dom as fully loaded first. The
  // page dom should always be loaded if we're running this behavior from AJAX/AHAH,
  // but during the initial document ready, it might not be yet, in which case, try
  // again after a small amount of time.
  if (!window.tinymce.dom.Event.domLoaded) {
    window.setTimeout(function() {Drupal.behaviors.tinymceAhah(context);}, 10);
    return;
  }
  
  // Now, we should be safe to create each editor instance.
  for (var sElementId in Drupal.settings.tinymceAhah) {
    if ($('#'+ sElementId + ':not(.tinymceAhah-processed)', context).size()) {
      // Get the settings to pass to the tinymce factory.
      var aElementSettings = Drupal.settings.tinymceAhah[sElementId];
      
      // During AJAX/AHAH, it might be possible that we're re-creating an
      // editor on a new element with the same id as an element that 
      // got removed from the DOM. I don't know whether anything bad happens
      // if we don't remove the old editor from the tinymce manager prior
      // to initializing the new one, but just in case, let's be clean about it.
      //
      // During initial page load (when the context is the document), if we 
      // have an editor, it's a result of an inline script having created it,
      // in which case, let's use it.
      var oOldEditor = window.tinyMCE.get(sElementId);
      if (oOldEditor && (context != document)) {
        window.tinyMCE.remove(oOldEditor);
      }
      
      // Create the editor instance for the element, unless it's the initial
      // page load, and we already have one.
      if (!oOldEditor || (context != document)) {
        window.tinyMCE.init(aElementSettings);
      }
      
      // Ensure we don't try to reapply this behavior on the same element.      
      $('#'+ sElementId, context).addClass('tinymceAhah-processed');
    }
  }
  
  // Ensure text inside tinymce editors is saved to the textarea
  // prior to a form being submitted with $.ajaxSubmit(). This 
  // requires version 2.16 or later of jquery.form.js.
  // We don't limit this to our context, because this will only run
  // after we've loaded tiny_mce.js, which might not be until 
  // we have our first element that needs it, and at that time, we'll
  // need to bind this functionality even to previously existing forms.
  $('form:not(.tinymceAhah-processed)').each(function() {
    $(this)
      .bind('form-pre-serialize', function() {window.tinyMCE.triggerSave();})
      .addClass('tinymceAhah-processed');
  });
  
};
